import React from 'react';
import './App.css';
import FinalResultPanel from "./containers/FinalResultPanel";
import ButtonPanel from "./components/ButtonPanel";

const App = () => (
    <div className=".calculator-body">
      <FinalResultPanel/>
      <ButtonPanel/>
    </div>
)

export default App;