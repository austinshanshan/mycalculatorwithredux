export const clickNumber = buttonName => ({
        type: 'ClICK_NUMBER',
        buttonName
})

export const clickOperator = buttonName => ({
        type: 'CLICK_OPERATOR',
        buttonName
})

export const clickCE = () => ({
        type: 'CLICK_CE'
})

export const clickEquation = () => ({
        type: 'CLICK_EQUATION'
})

export const clickC = () => ({
        type: 'CLICK_C'
})

export const clickDOT = () => ({
        type: 'CLICK_DOT'
})