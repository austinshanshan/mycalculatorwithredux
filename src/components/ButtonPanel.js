import React from 'react';
import {clickEquation, clickNumber, clickOperator, clickC, clickCE} from '../actions';
import { connect } from 'react-redux';

const ButtonPanel = ({ dispatch }) => {

        return (
            <div className="button">
                <button name="(" onClick={e => dispatch(clickNumber(e.target.name))}>(</button>
                <button name="CE" onClick={e => dispatch(clickCE(e.target.name))}>CE</button>
                <button name=")" onClick={e => dispatch(clickNumber(e.target.name))}>)</button>
                <button name="C" onClick={e => dispatch(clickC(e.target.name))}>C</button><br/>

                <button name="1" onClick={e => dispatch(clickNumber(e.target.name))}>1</button>
                <button name="2" onClick={e => dispatch(clickNumber(e.target.name))}>2</button>
                <button name="3" onClick={e => dispatch(clickNumber(e.target.name))}>3</button>
                <button name="+" onClick={e => dispatch(clickOperator(e.target.name))}>+</button><br/>

                <button name="4" onClick={e => dispatch(clickNumber(e.target.name))}>4</button>
                <button name="5" onClick={e => dispatch(clickNumber(e.target.name))}>5</button>
                <button name="6" onClick={e => dispatch(clickNumber(e.target.name))}>6</button>
                <button name="-" onClick={e => dispatch(clickOperator(e.target.name))}>-</button><br/>

                <button name="7" onClick={e => dispatch(clickNumber(e.target.name))}>7</button>
                <button name="8" onClick={e => dispatch(clickNumber(e.target.name))}>8</button>
                <button name="9" onClick={e => dispatch(clickNumber(e.target.name))}>9</button>
                <button name="*" onClick={e => dispatch(clickOperator(e.target.name))}>*</button><br/>

                <button name="." onClick={e => dispatch(clickNumber(e.target.name))}>.</button>
                <button name="0" onClick={e => dispatch(clickNumber(e.target.name))}>0</button>
                <button name="=" onClick={e => dispatch(clickEquation(e.target.name))}>=</button>
                <button name="/" onClick={e => dispatch(clickOperator(e.target.name))}>/</button><br/>
            </div>
        );

}

export default connect()(ButtonPanel);