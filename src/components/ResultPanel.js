import React from 'react';
import PropTypes from 'prop-types';

const ResultPanel = ({result}) => (
            <div className="result">
                <p>{result}</p>
            </div>
)

ResultPanel.prototype = {
    result: PropTypes.string.isRequired
}

export default ResultPanel;