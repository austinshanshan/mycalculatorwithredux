import { connect } from 'react-redux';
import ResultPanel from "../components/ResultPanel";

function mapStateToProps(state) {
    const result = state.Calculate.total || state.Calculate.next || '0'
    return {result}
}


export default connect(
    mapStateToProps,
    null
)(ResultPanel)