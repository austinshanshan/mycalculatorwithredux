import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import rootReducer from './reducers'
import FinalResultPanel from "./containers/FinalResultPanel";
import ButtonPanel from "./components/ButtonPanel";
import App from "./App";

const store = createStore(rootReducer)

render(
    <Provider store={store}>
        {/*<div className=".calculator-body">*/}
        {/*    <FinalResultPanel/>*/}
        {/*    <ButtonPanel/>*/}
        {/*</div>*/}
        <App/>
    </Provider>,
    document.getElementById('root')
)
