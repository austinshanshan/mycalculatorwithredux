const Calculate = (state = {}, action) => {
    switch (action.type) {
        case 'ClICK_NUMBER':
            return processIsNumber(state, action.buttonName);
        case 'CLICK_OPERATOR':
            return processIsOperator(state, action.buttonName);
        case 'CLICK_EQUATION':
            return processIsEquation(state);
        case 'CLICK_CE':
            return processIsCE(state);
        case 'CLICK_DOT':
            return processIsDOT(state);
        case 'CLICK_C':
            return processIsC(state);
        default:
            return state;
    }
}

const processIsC = (obj) => {
    if(obj.total) {
        return {
            total: obj.total.substring(0, obj.total.length - 1),
            next: obj.next,
            operation: obj.operation
        }
    } else {
        if(obj.next) {
            return {
                total: obj.total,
                next: obj.next.substring(0, obj.next.length -1),
                operation: obj.operation
            }
        }
    }

}

const processIsDOT = (obj) => {
    if(obj.total) {
        return {
            total: obj.total.concat('.'),
            next: obj.next,
            operation: obj.operation
        }
    } else {
        if(obj.next) {
            return {
                total: obj.total,
                next: obj.next.concat('.'),
                operation: obj.operation
            }
        }
    }

}
const processIsCE = (obj) => {
    return {
        total: null,
        next: null,
        operation: null
    }
}


const processIsEquation = (obj) => {
    return {
        total: operate(obj.operation, obj.total, obj.next),
        next: null,
        operation: null
    }
}

const processIsOperator = (obj, buttonName) => {
    return {
        total: obj.total,
        next: obj.next,
        operation: buttonName
    }
}

const processIsNumber = (obj, buttonName) => {
    if(obj.operation && obj.next) {
        if(obj.total) {
            return {
                total: obj.total + buttonName,
                next: obj.next,
                operation: obj.operation
            }
        } else {
            return {
                total: buttonName,
                next: obj.next,
                operation: obj.operation
            }
        }
    }
    if(!obj.operation && !obj.next && obj.total) {  // clean the result from previous calculation, save 1st operand in next
        return {
            total: null,
            next: buttonName,
            operation: null
        }
    }
    if(obj.next) {
        return {
            total: obj.total,
            next: obj.next + buttonName,
            operation: obj.operation
        }
    }
    return {
        total: obj.total,
        next: buttonName,
        operation: obj.operation
    }

}

function operate (operation, operand1, operand2) {
    if(operation === '+') {
        let op1 = operand1===null?0:Number(operand1);
        let op2 = operand2===null?0:Number(operand2);
        return op1+op2;
    }
    if(operation === '-') {
        let op1 = operand1===null?0:Number(operand1);
        let op2 = operand2===null?0:Number(operand2);
        return op2-op1;
    }
    if(operation === '*') {
        let op1 = operand1===null?0:Number(operand1);
        let op2 = operand2===null?0:Number(operand2);
        return op1*op2;
    }
    if(operation === '/') {
        let op1 = operand1===null?0:Number(operand1);
        let op2 = operand2===null?0:Number(operand2);
        return op2/op1;
    }
}

export default Calculate